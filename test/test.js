"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const chai = require('chai');
const expect = chai.expect;

const userSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName:{
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true
    }
});

const testSchema = new Schema({
    users: [userSchema],
    eventDate: {
        type: String,
        required: true
    }
});

const Events = mongoose.model('Events', testSchema);

describe('Database Tests', function(){

    before(function(done){
        mongoose.connect('mongodb://localhost/testDB');
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', function(){
            console.log('Connected to db');
            done();
        });
    });

    describe('Test DB', function(){
        it('New event saved to db', function(done){
            var testEvent = Events({
                users: [{
                    firstName: 'Jan',
                    lastName: 'Kowalski',
                    Email: 'jk@gmail.com'
                }],
                eventDate: '2020/1/1'
            });

            testEvent.save(done);
        });

        it('Dont save incorrect data ', function(done){
            var wrongSave = Events({
                wrong: 'WRONG'
            });

            wrongSave.save(err => {
                if(err) { return done(); }
                throw new Error('Should generate error!');
            });
        });

        it('Should retrieve data from DB', function(done){
            Events.find({eventDate: '2020/1/1'}, (err, mail) =>{
                if(err){throw err;}
                if(mail.length === 0){throw new Error('No data');}
                done();
            });
        });
    })

    after(function(done){
        mongoose.connection.db.dropDatabase(function(){
            mongoose.collection.close();
        });
        done();
    });

});