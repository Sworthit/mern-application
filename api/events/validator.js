const validator = require("validator");
const moment = require("moment");
const isEmpty = require("./isEmpty");


module.exports = function validInput(data) {
  let errors = {};
  let today = moment().toDate();
  data.firstName = !isEmpty(data.firstName) ? data.firstName : "";
  data.lastNamr = !isEmpty(data.lastName) ? data.lastName : "";
  data.Email = !isEmpty(data.Email) ? data.Email : "";
  data.eventDate = !isEmpty(data.eventDate) ? data.eventDate : "";

  if (validator.isEmpty(data.firstName)) {
    errors.firstName = "Name is required";
  }
  if (validator.isEmpty(data.lastName)) {
    errors.lastName = "Last name is required";
  }
  if (validator.isEmpty(data.Email)) {
    errors.Email = "Email is required";
  }
  if (validator.isEmpty(data.eventDate)) {
    errors.eventDate = "Date is required";
  }
  if (!validator.isEmail(data.Email)) {
    errors.Email = "Email is invalid";
  }
  if (moment(today).isAfter(data.eventDate)) {
    errors.eventDate = "Date is in past!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
