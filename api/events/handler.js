const express = require("express");
const router = express.Router();

const validInput = require("./validator");

const Events = require("../../models/Event");

router.post("/", async (req, res) => {
  
  const {errors, isValid}  = validInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  } else {
    try {
      const nEvent = new Events({
        users: [
          {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            Email: req.body.Email
          }
        ],
        eventDate: req.body.eventDate
      });
      let newEvent = await Events.findOne({
        eventDate: req.body.eventDate
      }).exec();
      if (!newEvent) {
        nEvent.save().then(() => {
          res.status(201).json("Added first user to that event");
        });
      } else {
        let eEvent = await Events.findOne({
          "users.Email": req.body.Email,
          eventDate: req.body.eventDate
        }).exec();
        if (eEvent) {
          errors.Email = "Email already enlisted to that event";
          return res.status(400).json(errors);
        } else {
          let uEvent = await Events.update(
            { eventDate: nEvent.eventDate },
            { $push: { users: nEvent.users } }
          ).exec();
          if (uEvent) {
            return res.status(201).json("Added new email to that event");
          }
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
});

module.exports = router;
