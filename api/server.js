const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const users = require("./events/handler");

async function run() {
  const db = require("../config/keys.json").mongoURI;

  mongoose
    .connect(
      db,
      { useNewUrlParser: true }
    )
    .then(() => console.log("MongoDb Connected"))
    .catch(err => console.log(err));
  const app = express();

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.get("/", (req, res) => res.send("MongoDb is active"));

  app.use("/api/register", users);

  const port = 5000;
  app.listen(port);
  console.log(`Listening at port: ${port}`);
}

run().catch(error => console.error(error.stack));
