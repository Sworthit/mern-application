import React from 'react'

const footer = () => (
  <footer className="bg-dark text-white text-center">
      Copyright &copy; {new Date().getFullYear()} 
  </footer>
)

export default footer;