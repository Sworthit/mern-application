import React, { Component } from "react";
import moment from "moment";
import SuccesAlert from "../layout/successAlert";
import ErrorAlert from "../layout/errorAlert";
import postToDb from "./postToDb";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      eventDate: new Date(),
      alertMessage: "",
      message: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  validate(eventDate) {
    const today = moment().toDate();
    if (moment(today).isAfter(eventDate)) {
      return "error";
    }
    return "";
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit(e) {
    const newUser = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      Email: this.state.email,
      eventDate: this.state.eventDate
    };
    e.preventDefault();
    const errors = this.validate(newUser.eventDate);
    if (errors.length > 0) {
      this.setState({
        alertMessage: errors,
        message: "Can't register event in past, or today!"
      });
    } else {
      postToDb(newUser)
        .then(res => {
          this.setState({ alertMessage: "success", message: res.data });
        })
        .catch(err => {
          this.setState({
            alertMessage: "error",
            message: err.response.data.Email
          });
        });
    }
    e.target.reset();
  }

  render() {
    return (
      <div className="register">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Sign Up</h1>
              <br />
              {this.state.alertMessage === "success" ? (
                <SuccesAlert message={this.state.message} />
              ) : null}
              {this.state.alertMessage === "error" ? (
                <ErrorAlert message={this.state.message} />
              ) : null}
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="First name"
                    name="firstName"
                    value={this.props.firstName}
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Last name"
                    name="lastName"
                    value={this.props.lastName}
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-group">
                  <input
                    type="email"
                    className="form-control form-control-lg"
                    placeholder="Email Address"
                    name="email"
                    value={this.props.email}
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    placeholder="Date"
                    name="eventDate"
                    value={this.props.eventDate}
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <input type="submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
