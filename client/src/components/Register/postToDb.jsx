import axios from "axios";

export default function postToDb(eventUser) {
  return axios.post("/api/register", eventUser);
}
