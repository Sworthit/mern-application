import React from "react";
import Register from "./components/Register/register.jsx";
import ReactDOM from "react-dom";
import App from "./App";
import Enzyme from "enzyme";
import { mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

describe("Basic page tests", () => {
  const wrapper = mount(<Register prop='jan' className = 'row'/>);
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should display firstName', () =>{
    
    expect(wrapper.props().prop).toBe('jan');
  });

  it('should have class', () => {
    expect(wrapper.hasClass('row')).toBe(true);
  })
});
