const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const UserSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName:{
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true
    },
});

const EventSchema = new Schema({
    users: [UserSchema],
    eventDate: {
        type: Date,
        required: true
    }
});

module.exports = Events = mongoose.model('events', EventSchema);